<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"             => "\":attribute\" musi zostać zaakceptowany.",
    "active_url"           => "\":attribute\" jest nieprawidłowym adresem URL.",
    "after"                => "\":attribute\" musi być datą późniejszą od :date.",
    "alpha"                => "\":attribute\" może zawierać jedynie litery.",
    "alpha_dash"           => "\":attribute\" może zawierać jedynie litery, cyfry i myślniki.",
    "alpha_num"            => "\":attribute\" może zawierać jedynie litery i cyfry.",
    "array"                => "\":attribute\" musi być tablicą.",
    "before"               => "\":attribute\" musi być datą wcześniejszą od :date.",
    "between"              => [
        "numeric" => "\":attribute\" musi zawierać się w granicach :min - :max.",
        "file"    => "\":attribute\" musi zawierać się w granicach :min - :max kilobajtów.",
        "string"  => "\":attribute\" musi zawierać się w granicach :min - :max znaków.",
        "array"   => "\":attribute\" musi składać się z :min - :max elementów.",
    ],
    "boolean"              => "\":attribute\" musi mieć wartość prawda albo fałsz",
    "confirmed"            => "Potwierdzenie \":attribute\" nie zgadza się.",
    "date"                 => "\":attribute\" nie jest prawidłową datą.",
    "date_format"          => "\":attribute\" nie jest w formacie :format.",
    "different"            => "\":attribute\" oraz \":other\" muszą się różnić.",
    "digits"               => "\":attribute\" musi składać się z :digits cyfr.",
    "digits_between"       => "\":attribute\" musi mieć od :min do :max cyfr.",
    "email"                => "Format \":attribute\" jest nieprawidłowy.",
    "exists"               => "Zaznaczony \":attribute\" jest nieprawidłowy.",
    "filled"               => "Pole \":attribute\" jest wymagane.",
    "image"                => "\":attribute\" musi być obrazkiem.",
    "in"                   => "Zaznaczony \":attribute\" jest nieprawidłowy.",
    "integer"              => "\":attribute\" musi być liczbą całkowitą.",
    "ip"                   => "\":attribute\" musi być prawidłowym adresem IP.",
    'json'                 => 'The \":attribute\" must be a valid JSON string.',
    "max"                  => [
        "numeric" => "\":attribute\" nie może być większy niż :max.",
        "file"    => "\":attribute\" nie może być większy niż :max kilobajtów.",
        "string"  => "\":attribute\" nie może być dłuższy niż :max znaków.",
        "array"   => "\":attribute\" nie może mieć więcej niż :max elementów.",
    ],
    "mimes"                => "\":attribute\" musi być plikiem typu :values.",
    "min"                  => [
        "numeric" => "\":attribute\" musi być nie mniejszy od :min.",
        "file"    => "\":attribute\" musi mieć przynajmniej :min kilobajtów.",
        "string"  => "\":attribute\" musi mieć przynajmniej :min znaków.",
        "array"   => "\":attribute\" musi mieć przynajmniej :min elementów.",
    ],
    "not_in"               => "Zaznaczony \":attribute\" jest nieprawidłowy.",
    "numeric"              => "\":attribute\" musi być liczbą.",
    "regex"                => "Format \":attribute\" jest nieprawidłowy.",
    "required"             => "Pole \":attribute\" jest wymagane.",
    "required_if"          => "Pole \":attribute\" jest wymagane gdy \":other\" jest :value.",
    "required_with"        => "Pole \":attribute\" jest wymagane gdy :values jest obecny.",
    "required_with_all"    => "Pole \":attribute\" jest wymagane gdy :values jest obecny.",
    "required_without"     => "Pole \":attribute\" jest wymagane gdy :values nie jest obecny.",
    "required_without_all" => "Pole \":attribute\" jest wymagane gdy żadne z :values nie są obecne.",
    "same"                 => "Pole \":attribute\" i \":other\" muszą się zgadzać.",
    "size"                 => [
        "numeric" => "\":attribute\" musi mieć :size.",
        "file"    => "\":attribute\" musi mieć :size kilobajtów.",
        "string"  => "\":attribute\" musi mieć :size znaków.",
        "array"   => "\":attribute\" musi zawierać :size elementów.",
    ],
    "string"               => "The \":attribute\" must be a string.",
    "timezone"             => "\":attribute\" musi być prawidłową strefą czasową.",
    "unique"               => "Taki \":attribute\" już występuje.",
    "url"                  => "Format \":attribute\" jest nieprawidłowy.",
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom'               => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes'           => [
        'email'                  => 'E-Mail',
        'name'                   => 'Nazwa',
        'first_name'             => 'Imię',
        'last_name'              => 'Nazwisko',
        'NIP'                    => 'NIP',
        'short_name'             => 'krótka nazwa',
        'reg'                    => 'Regulamin',
        'street'                 => 'Adres',
        'country'                => 'Kraj',
        'city'                   => 'Miasto',
        'venue'                  => 'Obiekt',
        'postal_code'            => 'Kod pocztowy',
        'tax[name]'              => 'Nazwa (na fakturze)',
        'tax[address]'           => 'Adres (na fakturze)',
        'tax[postal_code]'       => 'Kod pocztowy (na fakturze)',
        'tax[city]'              => 'Miasto (na fakturze)',
        'tax[country]'           => 'Kraj (na fakturze)',
        'tax[nip]'               => 'NIP (na fakturze)',
        'invoice'                => 'Faktura',
        'description'            => 'Opis',
        'country_id'             => 'ID Kraju',
        'language_id'            => 'ID Języka',
        'sex'                    => 'Płeć',
        'surname'                => 'Nazwisko',
        'phone_1'                => 'Telefon',
        'phone_2'                => 'Telefon',
        'fax'                    => 'Fax',
        'company_id'             => 'ID Organizatora',
        'facebook'               => 'Facebook',
        'twitter'                => 'Twitter',
        'linkedin'               => 'LinkedIn',
        'www'                    => 'WWW',
        'logo'                   => 'Logo',
        'published'              => 'Opublikowany',
        'color'                  => 'Kolor',
        'symbol'                 => 'Symbol',
        'default'                => 'Domyślny',
        'active'                 => 'Aktywny',
        'content'                => 'Treść',
        'sender'                 => 'Nadawca',
        'subject'                => 'Temat',
        'category_id'            => 'ID Kategorii',
        'email_id'               => 'ID Adresu E-mail',
        'email_template_id'      => 'ID Wzoru E-mail',
        'portfolio'              => 'Portfolio',
        'dictionary_crew_id'     => 'ID Słownika Ekipy',
        'slug'                   => 'Slug',
        'dictionary_category_id' => 'ID Kategorii',
        'title'                  => 'Tytuł',
        'dictionary_country_id'  => 'ID Kraju',
        'short_description'      => 'Krótki opis',
        'min_age'                => 'Minimalny wiek',
        'dictionary_language_id' => 'ID Języka',
        'duration'               => 'Czas trwania',
        'model'                  => 'Model',
        'owner_id'               => 'ID Właściciela',
        'type'                   => 'Typ',
        'src'                    => 'Źródło',
        'order'                  => 'Kolejność',
        'gallery_id'             => 'ID Galerii',
        'password'               => 'Hasło',

    ],

];
